<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://apollon.guru/
 * @since      1.0.0
 *
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/public
 * @author     Grin <grindmitriy@gmail.com>
 */
class Ag_Link_Estimate_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	private $IDNA;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->IDNA = new idna_convert();
		$this->add_shortcodes();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ag_Link_Estimate_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ag_Link_Estimate_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ag-link-estimate-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ag_Link_Estimate_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ag_Link_Estimate_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ag-link-estimate-public.js', array( 'jquery' ), $this->version, false );

	}

	private function add_shortcodes() {
		add_shortcode( 'ag_link_estimate', array( $this, 'link_estimate_shortcode' ) );
	}

	public function link_estimate_shortcode( $atts ){
		include 'partials/form.php';

		if(!array_key_exists('ag-el-urls', $_POST) || !$_POST['ag-el-urls'] || !wp_verify_nonce( $_POST['ag_estimate_name'], 'ag_estimate_action'))
			return;

		$this->get_url_estimate(stripcslashes($_POST['ag-el-urls']));
	}

	public function get_url_estimate($text)
	{
		global $wpdb;

		$tables = array($wpdb->prefix.'le_sape', $wpdb->prefix.'le_gogetlinks', $wpdb->prefix.'le_miralinks');

		$inserted_urls = $this->text_to_lines(strtolower($text));

		$inserted_urls_count = count($inserted_urls);

		$imploded_urls_for_query = strtolower("'".implode("','", $inserted_urls)."'");

		$amounts = array();
		
		$amounts['sape_min_price']['amount'] = 0;
		
		$query = '';
		
		foreach ($tables as $key => $table) {
			if($key)
				$query .= " UNION ";

			if($wpdb->prefix.'le_sape' == $table){
				$sape_query = ", sape_id, price_min as 'sape_min_price'";
			} else {
				$sape_query = ", '' as 'sape_id', '0' as 'sape_min_price'";
			}

			$query .= "SELECT url, price, '$table' as 'table' $sape_query FROM $table WHERE url IN (".$imploded_urls_for_query.")";

			$amounts[$table]['amount'] = 0;

			$amounts[$table]['count'] = 0;
		}

		$results = $wpdb->get_results($query, 'ARRAY_A');

		$total = 0;
		$count = 0;
		$data_for_print_tables = array();

		foreach($results as $row){

			$data_for_print_tables[$row['url']][$row['table']] = $row['price'];

			if(!@$data_for_print_tables[$row['url']]['sape_id'])
				$data_for_print_tables[$row['url']]['sape_id'] = $row['sape_id'];
			
			if(!@$data_for_print_tables[$row['url']]['sape_min_price'])
				$data_for_print_tables[$row['url']]['sape_min_price'] = $row['sape_min_price'];

			$amounts[$row['table']]['amount'] += $row['price'];
			$amounts[$row['table']]['count'] ++;
			$amounts['sape_min_price']['amount'] += $row['sape_min_price'];
		}

		$table_obj = array(
			'thead' => array(
				array('label'=> ''),
				array('label'=> 'URL'),
				array('css_class' => 'text-right', 'label'=> 'Sape ID'),
				array('css_class' => 'text-right', 'label'=> 'Sape min'),
				array('css_class' => 'text-right', 'label'=> 'Sape max'),
				array('css_class' => 'text-right', 'label'=> 'GoGetLinks'),
				array('css_class' => 'text-right', 'label'=> 'Miralinks'),
			),
			'tbody' => array()
		);

		$url_number = 0;
		foreach ($inserted_urls as $inserted_url){
			$url_number++;
			$table_obj['tbody'][] = array(
				array('label'=> $url_number),
				array('label'=> $this->IDNA->decode($inserted_url)),
				array('css_class' => 'text-right', 'label'=> @$data_for_print_tables[$inserted_url]['sape_id']),
				array('css_class' => 'text-right', 'label'=> number_format(@$data_for_print_tables[$inserted_url]['sape_min_price'],0,'',' ')),
				array('css_class' => 'text-right', 'label'=> number_format(@$data_for_print_tables[$inserted_url][$wpdb->prefix.'le_sape'],0,'',' ')),
				array('css_class' => 'text-right', 'label'=> number_format(@$data_for_print_tables[$inserted_url][$wpdb->prefix.'le_gogetlinks'],0,'',' ')),
				array('css_class' => 'text-right', 'label'=> number_format(@$data_for_print_tables[$inserted_url][$wpdb->prefix.'le_miralinks'],0,'',' ')),
			);
		}
		$table_obj['tbody'][] = array(
			array('label'=> 'Стоимость:'),
			array('label'=> ''),
			array('label'=> ''),
			array('css_class' => 'text-right', 'label'=> number_format(@$amounts['sape_min_price']['amount'],0,'',' ')),
			array('css_class' => 'text-right', 'label'=> number_format(@$amounts[$wpdb->prefix.'le_sape']['amount'],0,'',' ')),
			array('css_class' => 'text-right', 'label'=> number_format(@$amounts[$wpdb->prefix.'le_gogetlinks']['amount'],0,'',' ')),
			array('css_class' => 'text-right', 'label'=> number_format(@$amounts[$wpdb->prefix.'le_miralinks']['amount'],0,'',' ')),
		);
		$table_obj['tbody'][] = array(
			array('label'=> 'Ссылок:'),
			array('label'=> $inserted_urls_count),
			array('label'=> ''),
			array('label'=> ''),
		);

		$num_row_count_urls_in_table = count($table_obj['tbody']) - 1;

		foreach ($tables as $table) {
			$table_obj['tbody'][$num_row_count_urls_in_table][] = array(
				'css_class' => 'text-right',
				'label' => number_format(@$amounts[$table]['count'],0,'',' ')
			);
		}

		$table_obj['tbody'][] = array(
			array('label'=> '%'),
			array('label'=> '100%'),
			array('label'=> ''),
			array('label'=> ''),
		);

		$num_row_percents = count($table_obj['tbody']) - 1;

		foreach ($tables as $table) {
			$table_obj['tbody'][$num_row_percents][] = array(
				'css_class' => 'text-right',
				'label' => round(($amounts[$table]['count'] / $inserted_urls_count) * 100, 2) . ' %'
			);
		}
		?>
		<div class="table-wrapper mt-5">

			<?php echo $this->get_table($table_obj); ?>
			
			<?php $min_max_prices = $this->get_min_max_prices_of_links_list($data_for_print_tables);
			list($min, $max) = $x;
			$min = number_format($min_max_prices['min'], 0, '', ' ');
			$max = number_format($min_max_prices['max'], 0, '', ' ');

			$urls_count =  count($data_for_print_tables);
			$urls_count_percent = round( ($urls_count / $inserted_urls_count) * 100 );

			$about = 'Количество ссылок: <b>' . $urls_count . '</b>, что составляет <b>' . $urls_count_percent . '%</b> от ссылочной массы оценивается в диапазоне от <b>' . $min . ' до ' . $max . '</b> рублей.';
			
			echo $about;
			
			$data_for_download  = 'data:text/plain;charset=UTF-8,';
			$data_for_download .= $this->table_csv_file($table_obj);
			$data_for_download .= "\n" . $about;

			echo '<br><br><br><a class="button btn" href="'.esc_attr($data_for_download).'" download="apollonguru_links_estimate.txt">Скачать csv</a>';
			?>

			<script src="https://apis.google.com/js/platform.js" async defer></script>
			<div class="g-savetodrive"
			   data-src="<?php echo esc_attr($data_for_download);?>"
			   data-filename="apollonguru_links_estimate.txt"
			   data-sitename="apollonguru_links_estimate">
			</div>
		</div>

		
		<?php
		
	}

	public function text_to_lines($text)
	{
		if(!$text) return;

		$text = trim($text);

		$lines = explode("\n", $text);

		$lines = array_map('trim', $lines);

		$lines = array_filter($lines);

		$lines = array_map(array($this, 'trim_quotes'), $lines);

		$lines = array_map(array($this, 'del_unable_start_symbols'), $lines);

		$lines = array_map(array($this, 'get_domain_name_from_url'), $lines);

		$lines = array_map(array($this, 'remove_www'), $lines);

		$lines = array_map(array($this, 'idna_convert'), $lines);

		return $lines;
	}

	public function get_domain_name_from_url($url){
		$parseUrl = parse_url(trim($url));
		return trim(@$parseUrl['host'] ? @$parseUrl['host'] : explode('/', $parseUrl['path'], 2)[0]);
	}

	public function del_unable_start_symbols($str)
	{
		$str = preg_replace('/^[^A-Za-z0-9А-Яа-я]+/u', '' ,$str);
		return $str;
	}

	public function trim_quotes($str){

		return trim($str, "'\"");
	}

	public function remove_www($url){
		return str_replace('www.', '', $url);
	}

	public function idna_convert($url){
		if(!!preg_match('/[а-яА-Я]/u', $url)){
			$url = $this->IDNA->encode($url);
		}
		return $url;
	}

	public function get_min_max_prices_of_links_list($data_for_print_tables)
	{
		global $wpdb;
		$total = array ('min' => 0, 'max' => 0);
		$fields = array($wpdb->prefix.'le_sape', 'sape_min_price', $wpdb->prefix.'le_gogetlinks', $wpdb->prefix.'le_miralinks');
		foreach ($data_for_print_tables as $url => $url_data) {
			$max = 0;
			$min = null;
			$prices = array();
			foreach ($fields as $field) {
				if(!array_key_exists($field, $url_data))
					continue;
				if(!$url_data[$field])
					continue;
				$max = max($max,$url_data[$field]);
				if(is_null($min))
					$min = $url_data[$field];
				else
					$min = min($min, $url_data[$field]);
			}
			if(is_null($min))
				$min = 0;

			$total['min'] += $min;
			$total['max'] += $max;
		}
		return $total;
	}

	public function get_table($table_arr)
	{
		// $html  = '<div class="table-wrapper mt-5">';
		$html .= '<table class="table table-striped">';

		if($table_arr['thead']) {
			
			$html .= '<thead> <tr>';
			
			foreach ($table_arr['thead'] as $thead_cell) {
				$html .= $this->get_table_cell($thead_cell, 'th');
			}
			
			$html .= '</tr> </thead>';
		}

		$html .= '<tbody>';

		foreach ($table_arr['tbody'] as $row) {
			$html .= '<tr>';
			foreach ($row as $cell) {
				$html .= $this->get_table_cell($cell);
			}
			$html .= '</tr>';
		}

		$html .= '</tbody>';
		
		$html .= '</table>';
		// $html .= '</div>';

		return $html;
	}

	public function get_table_cell($cell, $tag = 'td')
	{
		$css_class = '';
		if(array_key_exists('css_class', $cell))
			$css_class = ' class="'.$cell['css_class'].'"';
		$html = '<' . $tag . $css_class . '>' . $cell['label'] . '</' . $tag . '>';
		return $html;
	}

	public function table_csv_file($table_arr)
	{
		$csv_file = '';
		$csv_arr = array();

		$separator = ',';

		if($table_arr['thead']) {
			$thead_cells_count = count($table_arr['thead']);
			for ($i=0; $i < $thead_cells_count-1; $i++) { 
				$csv_file .= $table_arr['thead'][$i]['label'] . $separator;
			}
			$csv_file .= $table_arr['thead'][$thead_cells_count-1]['label'] . "\n";
		}

		foreach ($table_arr['tbody'] as $row) {

			$cells_count = count($row);
			
			for ($i=0; $i < $cells_count-1; $i++) { 
			
				$csv_file .= $row[$i]['label'] . $separator;
			
			}

			$csv_file .= $row[$cells_count-1]['label'] . "\n";
			
			// $csv_file .= "\n";
		}

		return $csv_file;

	}

}
