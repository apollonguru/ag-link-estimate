<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://apollon.guru/
 * @since      1.0.0
 *
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
