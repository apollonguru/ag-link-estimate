<form method="post">
	<div class="form-group">
		<label for="links-list">Urls</label>
		<?php
		$text = "";
		if( array_key_exists('ag-el-urls', $_POST) ) $text = stripcslashes($_POST['ag-el-urls']); ?>
		<textarea id="links-list" rows="10" name="ag-el-urls" style="width:100%;max-width: 575px;"><?php echo $text; ?></textarea>
		<?php wp_nonce_field('ag_estimate_action','ag_estimate_name'); ?>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>