(function( $ ) {
	'use strict';

	$(document).ready(function() {

		$('#links-list').on('input', function(event) {
			let text = $(this).val();
			text = text.replace(/^[^A-Za-z0-9А-Яа-я\s]+/gm, '');
			text = text.replace(/^\w*:\/\//gm, '');
			text = text.replace(/\/.*/gm, '');
			
			// let lines = text.split("\n");
			// lines = lines.filter(line => line.search(/\./)+1  );
			// lines = lines.map(line => line.replace(/\s/gm, ''));
			
			// text = lines.join("\n");
			$(this).val(text);
		});
	});

})( jQuery );
