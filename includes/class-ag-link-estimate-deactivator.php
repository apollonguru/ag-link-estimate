<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://apollon.guru/
 * @since      1.0.0
 *
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/includes
 * @author     Grin <grindmitriy@gmail.com>
 */
class Ag_Link_Estimate_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		global $wpdb;

		$tables = array('le_sape', 'le_gogetlinks', 'le_miralinks');

		foreach ($tables as $table) {
			
			$table_name = $wpdb->prefix . $table;
			
			$sql = "DROP TABLE IF EXISTS $table_name";
			
			$wpdb->query($sql);
			
		}

		delete_option("ag_le_db_version");

	}

}
