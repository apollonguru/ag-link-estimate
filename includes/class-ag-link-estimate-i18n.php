<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://apollon.guru/
 * @since      1.0.0
 *
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/includes
 * @author     Grin <grindmitriy@gmail.com>
 */
class Ag_Link_Estimate_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ag-link-estimate',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
