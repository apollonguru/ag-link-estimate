<?php

/**
 * Fired during plugin activation
 *
 * @link       https://apollon.guru/
 * @since      1.0.0
 *
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ag_Link_Estimate
 * @subpackage Ag_Link_Estimate/includes
 * @author     Grin <grindmitriy@gmail.com>
 */
class Ag_Link_Estimate_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		$ag_le_db_version = '1.0';
		
		$charset_collate = $wpdb->get_charset_collate();

		$tables = array(
			'sape' => "CREATE TABLE {$wpdb->prefix}le_sape (
				url varchar(190) NOT NULL,
				price decimal(10,2) NOT NULL DEFAULT '0.00',
				price_min decimal(10,2) NOT NULL DEFAULT '0.00',
				sape_id int(10) UNSIGNED NOT NULL,
				PRIMARY KEY (url)
			) $charset_collate;",

			'gogetlinks' => "CREATE TABLE {$wpdb->prefix}le_gogetlinks (
				url varchar(190) NOT NULL,
				price decimal(10,2) NOT NULL,
				PRIMARY KEY (url)
			) $charset_collate;",

			'miralinks' => "CREATE TABLE {$wpdb->prefix}le_miralinks (
				url varchar(190) NOT NULL,
				price decimal(10,2) NOT NULL,
				PRIMARY KEY (url)
			) $charset_collate;",
		);

		foreach ($tables as $sql) {

			dbDelta( $sql );

		}

		add_option( 'ag_le_db_version', $ag_le_db_version );
	}

}
